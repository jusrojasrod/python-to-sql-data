This funtion is intended to show you a "equivalent" datatypes between the ones show by pandas dtype funtion and the ones in SQL, i.e. float64 could be mapped to DECIMAL(L, d), where L stands to number Lentgh and d is for decimal positions. 

Information related to sql datatypes to help assigning them when creating a table in SQL.

Returns a table with the next columns:

    'Datatype', 'Maximum Length', 'Value', 'Datatype Recommended'
    
-Dataype: datatype as identify by pandas.

-Maximum Length: Maximum datatype lenght find in each column.

-Value: data with maximum lenght.

-Datatype Recommended: Datatype that should be use in SQL as "equivalent" 
                       from the one given by pandas (Datatype column).
