
import pandas as pd


def data_info_to_sql(df):
    """
    Information related to sql datatypes to help assigning them when creating a
    table in SQL.

    Returns a table with the next columns
    'Datatype', 'Maximum Length', 'Value', 'Datatype Recommended'
    
    -Dataype: datatype as identify by pandas.
    -Maximum Length: Maximum datatype lenght find in each column.
    -Value: data with maximum lenght.
    -Datatype Recommended: Datatype that should be use in SQL as "equivalent" 
                           from the one given by pandas (Datatype column).
    """


    column_info = {}
    columns = ['Datatype', 'Maximum Length', 'Value', 'Datatype Recommended']
    
    data_info = pd.DataFrame(columns=columns)
    
    
    # Populate Datatype column
    data_info['Datatype'] = [str(dtype) for dtype in df.dtypes]
    
    
    # Populate Maximum Length and Values Columns
    columns = df.columns

    values = {'Max_length':[], 'Value':[]}

    for name in columns:
        max_len = 0
        value = 0
        column = df[name]

        for i in range(column.shape[0]):
            if len(str(column[i])) > max_len:
                max_len = len(str(column[i]))
                value = column[i]

        values['Max_length'].append(max_len) 
        values['Value'].append(value) 
        
    data_info['Maximum Length'] = values['Max_length']
    data_info['Value'] = values['Value']
    
    
    # Datatype recommended
    Datatype_Recommended = []
    
    for index, item in enumerate(data_info['Datatype']):
        if item == 'float64':
            float_to_string = str(data_info['Value'][index])
            string_ = float_to_string.split('.')
            Datatype_Recommended.append('DECIMAL({},{})'.format(data_info.iloc[index, 
                                                                1], len(string_[1])))
        elif item == 'int64':
            Datatype_Recommended.append('VARCHAR({})'.format(data_info.iloc[index, 1]))
        elif item == 'object':
            Datatype_Recommended.append('CHAR({})'.format(data_info.iloc[index, 1]))
        else:
            Datatype_Recommended.append('Not a simple string-type')
    
    data_info['Datatype Recommended'] = Datatype_Recommended  
    
    # Return final dataframe
    data_info.index = list(df.columns)
    return data_info



def create_table_sql(data_frame, table_name):
    dt_re = iter(data_frame['Datatype Recommended'])
    col_definition = ["CREATE TABLE {}(".format(table_name)]
    
    for column in list(data_frame.index):
        col_definition.append('{0} {1},'.format(column, next(dt_re)))
                
    #head_statement = "CREATE TABLE" + str(table_name) 
    col_definition.append("PRIMARY KEY(**DEFINE KEY**) \n )")
    return col_definition  


if __name__ == "__main__":
    
    df = pd.read_csv('ChicagoCrimeData.csv')
    info_df = data_info_to_sql(df)
    print(info_df)

    sql_table = create_table_sql(info_df, "CHICAGO_PUBLIC_SCHOOLS")
    print(sql_table[0])
    for line in sql_table[1:]:
        print("\t\t", line) 


